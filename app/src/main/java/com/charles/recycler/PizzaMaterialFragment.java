package com.charles.recycler;




import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


public class PizzaMaterialFragment extends Fragment {

    public PizzaMaterialFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     final RecyclerView pizzaRecycler = (RecyclerView)inflater.inflate(
                R.layout.fragment_pizza_material, container, false);
        try{
            Ion.with(getActivity())
                    .load("http://api.androidhive.info/contacts")
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if(result!=null)
                            {
                                Toast.makeText(getContext(),"Connected",Toast.LENGTH_LONG).show();
                                try {

                                    JsonArray jsonArray = result.getAsJsonArray("contacts");

                                    String[] name = new String[jsonArray.size()];
                                    String[] email = new String[jsonArray.size()];

                                    for(int i=0;i<jsonArray.size();i++)
                                    {
                                        JsonObject contact=jsonArray.get(i).getAsJsonObject();
                                        name[i] =contact.get("name").getAsString();
                                        email[i] =contact.get("email").getAsString();

                                    }
                                    CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(name,email);
                                    pizzaRecycler.setAdapter(adapter);
                                }
                                catch(Exception f){
                                    Toast.makeText(getContext(),"cannot get as string",Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(getContext(), "No Internet ....", Toast.LENGTH_LONG).show();
                                String[] Name = new String[1];
                                String[] Email = new String[1];
                                Name[0]="Maria Charles";
                                Email[0]="m.charles161@gmail.com";
                                CaptionedImagesAdapter adapter = new CaptionedImagesAdapter( Name, Email);
                                pizzaRecycler.setAdapter(adapter);
                            }

                        }
                    });
        }
        catch(Exception e){
            Toast.makeText(getContext(),"Program Error",Toast.LENGTH_LONG).show();

        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(1);
        pizzaRecycler.setLayoutManager(layoutManager);
        return pizzaRecycler;
    }

}

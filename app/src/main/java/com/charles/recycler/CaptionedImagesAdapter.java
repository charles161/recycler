package com.charles.recycler;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.support.v7.widget.CardView;
import android.widget.TextView;



class CaptionedImagesAdapter extends RecyclerView.Adapter<CaptionedImagesAdapter.ViewHolder>{

    private String[] title;
    private String[] captions;


    public CaptionedImagesAdapter(String[] title,String[] captions){
        this.title=title;
        this.captions = captions;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public CaptionedImagesAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType){
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_captioned_image, parent, false);
        return new ViewHolder(cv);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        CardView cardView = holder.cardView;
       final TextView tit = (TextView)cardView.findViewById(R.id.info_title);
        tit.setText(title[position]);
        TextView cap = (TextView)cardView.findViewById(R.id.info_text);
        cap.setText(captions[position]);
    }
    @Override
    public int getItemCount(){
        return captions.length;
    }
}